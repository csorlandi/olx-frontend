import React from 'react';

import { Container } from './styles';

export default function About() {
  return (
    <Container>
      <h1>Sobre</h1>
    </Container>
  );
}
